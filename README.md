# VT100 miniatures 

This is a public repository to support the 1:10 scale and 1:3 scale VT100s made by [Trevor Flowers](https://transmutable.com/).

If you have a smol 1:10 scale VT100 then you will find printable screens in the [tenth-scale-files](./tenth-scale-files/) directory.

If you have one of the larger 1:3 scale VT100 terminals then head over to [third-scale-files](./third-scale-files/).

