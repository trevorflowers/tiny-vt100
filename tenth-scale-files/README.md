# 1:10 scale VT100 info

This information is for the tiny VT100 miniature. If you have the larger working VT100 terminal then head over to the [third-scale-files README](../third-scale-files/README.md).

The SVG files in thi dierctory provide printable example screens to use in the smol terminal model.

To make your own, you'll probably want the DEC Terminal Modern font from [dafont.com](https://www.dafont.com/dec-terminal-modern.font).
