# 1:3 scale VT100 HOWTO

This document covers the details of setting up one of Trevor Flower's [Small Electronic VT100 terminals](https://store.transmutable.com/l/small-vt-100) as a working terminal. It comes out of the box as aa nice addition to any collection but it can be more if you so desire.

What's in the box:
- 3D printed case for the terminal
- single board computer mount (inside the case)
- 3.5" digital display (installed)
- HDMI to micro HDMI cable
- micro-USB to USB-A cable (power for the display)
- working 3D printed USB keyboard
- coiled micro-USB to USB-A cable (for keyboard)
- DB9 port elbow

To enable the small VT100 as a terminal, you'll need a single board computer (SMB) and a few cables.  

If you don't have a preference, the best supported SMB is the Raspberry Pi. As of this edit, the Raspberry Pi 5 is the latest board but a v4 or v3 will also work. There are many other SMBs that are the same size and general capabilities of the Raspberry Pi 5 from manufacturers like [Banana Pi](https://banana-pi.org/en/banana-pi-sbcs/) and [Raxda](https://radxa.com/). If you choose one that runs Linux then these instructions should be a good start but you'll need to tweak them for your specific board.

## Getting into the main and keyboard cases

Though there are bolts visible on the back and bottom of the main case, you don't use them to open it up. In the envelope you received with the VT100 there are two guitar picks to use as prying tools.

Similarly, the keyboard is opened with a quick pry on the back.

Here's a video showing how to open both and describing what you'll find inside: [Inside the Mini VT100](https://youtu.be/4R-wjYxLGC0)

## Installing a Raspberry Pi 5

Here's the list of the hardware you'll need for a Raspberry Pi 5 build in the small VT100:
- Raspberry Pi 5
- Raspberry Pi 5 active cooler
- Official 5A Raspberry Pi Power Supply
- micro SD card, at least 16GB

I recommend installing the Raspberry Pi active cooler for this build. There are bottom and top vents on the VT100 but the Raspberry Pi is a hot little board and active air movement ensures that you won't melt the 3D prints!

I recommend the official RPi power supply because it's guarenteed to provide enough power to drive the RPi5, the active cooler, the display, and any peripherals like a USB hub or a USB to serial port dongle. You can use another USB power supply and cable but be certain that it is USB-C (for the RPi 5) and rated for the same 27 Watts. Using a lower rated power supply will result in the RPi protecting itself by slowing down the processor and even turning off peripherals.

Use you personal computer to write the Raspberry PI OS to your SD card. By far the easiest method is to use the [Raspberry Pi Imager](https://www.raspberrypi.com/software/) on your current computer. It will let you choose your RPi, the OS, and then it will automatically download and write the correct image. Be sure to take advantage of Imager's option to configure the OS for your network, as well as with a user account with the username of your choice. You can do all of that later but it's less futzy to configure it up front.

## Modifying the keyboard code

Inside the keyboard are two custom PCBs, one for the key grid and one for the LEDs. A [Teensy 4.1](https://www.pjrc.com/store/teensy41.html) microcontroller board is soldered to the key grid PCB.

The Arduino code in the 'keyboard-ino/SwitchBoard.ino' file is what ships on the keyboard. The comments in that file explain the serial protocol for controlling the LEDs. To modify the key mapping you'll need to edit this file and then follow the Teensy 4.1 [instructions for installing the new code](https://www.pjrc.com/teensy/td_download.html).

