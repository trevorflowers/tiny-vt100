/*
This is the control code for the 1:3 scale VT100 keyboard made by Trevor Flowers.

The keyboard is exposed as a USB keyboard and a USB serial port.

To remap a key to a different value you can edit the keyValues 2D array.

Keys that don't have an Arduino key code are "special" and will cause a key event over the serial port. 

Reading and setting the LEDs happen similarly over the serial port. See the command variables below. 
*/

const String SET_LED_COMMAND = "SET_LED"; // set the sixth LED on example: SET_LED 5 1
const String SET_LEDS_COMMAND = "SET_ALL_LEDS"; // set the second LED on and the rest off: SET_LEDS 0100000
const String READ_LEDS_COMMAND = "READ_LEDS"; // read LED values example returns seecond LED on: 0100000
const String BUZZ_MOTOR_COMMAND = "BUZZ_MOTOR"; // buzz the motor for 45 ms example: BUZZ_MOTOR 45
const String SPECIAL_KEY_EVENT = "SPECIAL_KEY"; // Sent with a unique integer for non-standard keys

const int MOTOR_PIN = 41;
const int MOTOR_KEY_PRESS_BUZZ_DURATION = 30; // ms

const int NUM_LEDS = 7;
int ledPins[NUM_LEDS] = {0, 1, 2, 3, 4, 5, 6};
// LED values, false is off
bool ledValues[NUM_LEDS] = {true, false, false, false, false, false, false};

// Keyboard pin values
const byte ROWS = 5;
const byte COLS = 19;
byte rowPins[ROWS] = {35, 36, 37, 38, 39};
byte colPins[COLS] = {14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32};

/*
HID values of keys.
-1 represents a place in the grid with no key.
Any other negative number is a special key with no default value.
*/
int keyValues[ROWS][COLS] = {
  {-2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, KEY_UP_ARROW, KEY_DOWN_ARROW, KEY_LEFT_ARROW, KEY_RIGHT_ARROW, KEY_F1, KEY_F2, KEY_F3, KEY_F4},
  {KEY_ESC, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9, KEY_0, KEY_MINUS, KEY_EQUAL, KEY_TILDE, KEY_BACKSPACE, 55, 56, 57, 45},
  {KEY_TAB, -1, KEY_Q, KEY_W, KEY_E, KEY_R, KEY_T, KEY_Y, KEY_U, KEY_I, KEY_O, KEY_P, KEY_LEFT_BRACE, KEY_RIGHT_BRACE, KEY_DELETE, 52, 53, 54, 44},
  {KEY_LEFT_CTRL, KEY_CAPS_LOCK, KEY_A, KEY_S, KEY_D, KEY_F, KEY_G, KEY_H, KEY_J, KEY_K, KEY_L, KEY_SEMICOLON, KEY_QUOTE, KEY_RETURN, KEY_BACKSLASH, 49, 50, 51, -1},
  {KEY_SCROLL_LOCK, KEY_LEFT_SHIFT, KEY_Z, KEY_X, KEY_C, KEY_V, KEY_B, KEY_N, KEY_M, 44, 46, 47, KEY_RIGHT_SHIFT, 10, -3, 48, 46, KEY_ENTER, KEY_SPACE}
};

// Used to track which keys are pressed
bool keyIsDown[ROWS][COLS] = {
  {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
  {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
  {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
  {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
  {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}
};

void setup() {
  Serial.begin(38400);
  Serial.setTimeout(10);
  Keyboard.begin();

  // Set up keys
  for(int r=0; r < ROWS; r++){
    pinMode(rowPins[r], OUTPUT);
    digitalWrite(rowPins[r], HIGH);
  }
  for(int c=0; c < COLS; c++){
    pinMode(colPins[c], INPUT_PULLUP);
  }

  // Set up motor
  pinMode(MOTOR_PIN, OUTPUT);
  digitalWrite(MOTOR_PIN, LOW);

  // Set up LEDs
  for(int l=0; l < NUM_LEDS; l++){
    pinMode(ledPins[l], OUTPUT);
    digitalWrite(ledPins[l], LOW);
  }

  logln("1:3 VT100 keyboard by Trevor Flowers initialized");
}

void log(String message) {
  Serial.print("# " + message);
}
void logln(String message){
  Serial.println("# " + message);
}

void sendKeyEvent(bool isDown, int row, int col) {
  int keyVal = keyValues[row][col];
  bool isSpecial = keyVal <= 0; // This will be true if it's a VT100-specific key like SET-UP or LINE FEED
  // logln(String(row) + ":" + String(col) + " " + String(keyVal) + " " + isDown);
  if(isDown){
    if(isSpecial){
      Serial.println(SPECIAL_KEY_EVENT + ": " + String(-1 * keyVal));
    } else {
      Keyboard.press(keyVal);
      delay(5);
    }
  } else {
    if(isSpecial){
      Serial.println(SPECIAL_KEY_EVENT + ": " + String(-1 * keyVal));
    } else {
      Keyboard.release(keyVal);
    }   
  }
}

void buzzMotor(int duration) {
    digitalWrite(MOTOR_PIN, HIGH);
    delay(duration);
    digitalWrite(MOTOR_PIN, LOW);
}

void handleInput(String input){
  if(input.length() == 0) {
    return;
  }
  if(input.startsWith(SET_LED_COMMAND + " ")){
    long index = input.substring(SET_LED_COMMAND.length() + 1, SET_LED_COMMAND.length() + 2).toInt();
    bool value = input.substring(SET_LED_COMMAND.length() + 3, SET_LED_COMMAND.length() + 4) == "1";
    if(index >= NUM_LEDS){
      logln("# LED index must be less than " + String(NUM_LEDS));
    } else {
      ledValues[index] = value;
    }
  } else if(input.startsWith(SET_LEDS_COMMAND + " ")) {
    String values = input.substring(SET_LEDS_COMMAND.length() + 1);
    for(uint i=0; i < values.length(); i++){
      ledValues[i] = values[i] == '1';
    }
  } else if(input.startsWith(READ_LEDS_COMMAND)) {
    String values = "0000000";
    for(uint i=0; i < NUM_LEDS; i++){
      if(ledValues[i]){
        values[i] = '1';
      }
    }
    Serial.println("LEDS " + values);
  } else if(input.startsWith(BUZZ_MOTOR_COMMAND + " ")) {
    int duration = input.substring(BUZZ_MOTOR_COMMAND.length() + 1).toInt();
    if (duration > 0) {
      buzzMotor(duration);
    }
  } else {
    logln("unknown: " + input);
  }
}

void loop() {
  bool block = false;
  bool keyFound = false;
  for(int r=0; r < ROWS; r++){
    digitalWrite(rowPins[r], LOW);
    for(int c=0; c < COLS; c++){
      if(block && (c == 0 || c == 2) ){
        continue;
      }
      if(digitalRead(colPins[c]) == LOW){
        if(keyIsDown[r][c] == false){
          keyFound = true;
          keyIsDown[r][c] = true;
          sendKeyEvent(true, r, c);
        }
        if(c == 0 || c == 2){
          block = true;
        }
      } else {
        if(keyIsDown[r][c] == true){
          keyFound = true;
          sendKeyEvent(false, r, c);
        }
        keyIsDown[r][c] = false;
      }
    }
    digitalWrite(rowPins[r], HIGH);
  }
  if(keyFound){
    buzzMotor(MOTOR_KEY_PRESS_BUZZ_DURATION);
  }
  // Update LED state
  for(int l=0; l < NUM_LEDS; l++){
    if(ledValues[l]) {
       digitalWrite(ledPins[l], HIGH);
    } else {
       digitalWrite(ledPins[l], LOW);
    }
  }
  handleInput(Serial.readStringUntil('\n'));
}

